package xRootdprotocolMockServer

import(
	"os"
  "fmt"
)


func checkError(err error) {
    if err != nil {
        fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
        os.Exit(1)
    }
}
