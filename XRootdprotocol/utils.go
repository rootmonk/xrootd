package XRootdprotocol

import(
  "encoding/binary"
)

//preapre byte of length 24
func preparevalue() []byte {
    return make([]byte,24)
}

//prepare InitialHandshake request
func  PrepareInitialHandshake() []byte{

  prepareInitialHandshakevalue := make([]byte,20)

  InitialHandshake := []uint32{
                    0,    //kXR_int32 0
                    0,    //kXR_int32 0
                    0,    //kXR_int32 0
                    4,    //kXR_int32 4 (network byte order)
                    2012 } // kXR_int32 2012(network byte order


  for i, v  := range InitialHandshake {
		binary.BigEndian.PutUint32(prepareInitialHandshakevalue[4*i:], v)
	}
	return prepareInitialHandshakevalue
}
